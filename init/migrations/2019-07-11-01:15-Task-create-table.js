'use strict';

module.exports = {
    up : (queryInterface, Sequelize) => {
        return queryInterface.createTable('Tasks', {
            id           : { type: Sequelize.UUID, defaultValue: Sequelize.UUIDV4, primaryKey: true },
            userId       : { type: Sequelize.UUID, allowNull: false, references: { model: "Users", key: "id" } },
            text         : { type: Sequelize.STRING },
            status       : { type: Sequelize.ENUM, values: [ "SUCCESSFUL", "FAILED", "PENDING" ], defaultValue: "PENDING" },
            createdAt    : { type: Sequelize.DATE, allowNull: false },
            updatedAt    : { type: Sequelize.DATE, allowNull: false }
        });
    },

    down : (queryInterface) => {
        return queryInterface.dropTable('Tasks');
    }
};
