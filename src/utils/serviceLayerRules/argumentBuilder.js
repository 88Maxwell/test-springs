import camelcase from "camelcase";

export default {
    name    : "argumentBuilder",
    type    : "hidden",
    execute : ctx => {
        const args = camelcaseObject({
            context : ctx.context,
            ...ctx.request.body,
            ...ctx.params,
            ...ctx.query
        });

        return args;
    }
};


function camelcaseObject(obj) {
    const resultObject = {};
    const keys = Object.keys(obj);

    keys.forEach(key => {
        const value = typeof obj[key] === "object" && !Array.isArray(obj[key])
            ? camelcaseObject(obj[key])
            : obj[key];

        resultObject[camelcase(key)] = value;
    });

    return resultObject;
}
