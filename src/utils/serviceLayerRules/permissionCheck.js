import jwt           from "jsonwebtoken";
import { promisify } from "bluebird";
import { Exception } from "koa-service-layer";
import { userDump } from "../services/dumps";
import sequelize from "../../sequelize";
import config from "../../../configs/config.json";

const jwtVerify = promisify(jwt.verify);
const User = sequelize.model("User");

export default {
    name    : "permissionCheck",
    type    : "custom",
    execute : async (ctx, args) => {
        if (args) {
            const token = ctx.request.get("X-AuthToken");

            if (token) {
                const { id } = await jwtVerify(token, config.SECRET);
                const user = await User.findByPk(id);

                if (user) return ({ ...ctx, context: { user: userDump(user) } });
            }

            throw new Exception({
                code   : "PERMISSION_DENIED",
                fields : {
                    token : "WRONG_TOKEN"
                }
            });
        }
    }
};

