import validate from "./validate";
import argumentBuilder from "./argumentBuilder";
import permissionCheck from "./permissionCheck";

export default [
    permissionCheck,
    argumentBuilder,
    validate
];

