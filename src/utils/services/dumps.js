export const userDump = obj => ({
    id    : obj.id,
    name  : obj.name,
    email : obj.email
});

export const taskDump = obj => ({
    id     : obj.id,
    userId : obj.userId,
    text   : obj.text,
    status : obj.status
});

