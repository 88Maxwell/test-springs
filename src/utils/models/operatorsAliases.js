import Sequelize from "sequelize";

export default {
    $not : Sequelize.Op.not,
    $col : Sequelize.Op.col
};

