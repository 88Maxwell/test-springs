import operatorsAliases from "./operatorsAliases";
import init from "./init";

export default {
    init,
    operatorsAliases
};
