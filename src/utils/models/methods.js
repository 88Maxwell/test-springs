import bcrypt         from "bcrypt";
import { saltRounds } from "../../../configs/config";

export default {
    password : {
        checkPassword(plain) {
            return new Promise((resolve, reject) => {
                bcrypt.compare(plain, this.passwordHash, (err, res) => {
                    if (err) reject(err);
                    resolve(res);
                });
            });
        },

        encryptPassword : pass => {
            const salt = bcrypt.genSaltSync(saltRounds); // eslint-disable-line no-sync

            return bcrypt.hashSync(pass, salt); // eslint-disable-line no-sync
        }
    },

    inject : (targetClass, methods) => {
        for (const method in methods) {
            if (methods.hasOwnProperty(method)) {
            // eslint-disable-next-line no-param-reassign
                targetClass.prototype[method] = methods[method];
            }
        }
    }
};
