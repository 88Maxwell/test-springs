import models from "../../models";
import methods from "./methods";

export default (sequelize, Sequelize) => {
    for (const modelKey in models) {
        if (models.hasOwnProperty(modelKey)) {
            const model = models[modelKey](sequelize, Sequelize);
            const modelName = model.options.name.singular;

            if (model.hasOwnProperty("initRelation")) model.initRelation();
            if (modelName === "User") methods.inject(model, methods.password);
        }
    }
};
