import { Service, Exception } from "koa-service-layer";
import jwt from "jsonwebtoken";
import config from "../../../configs/config.json";
import sequelize from "../../sequelize";
import { userDump } from "../../utils/services/dumps";

const User = sequelize.model("User");

export default class Create extends Service {
    static validate = {
        data : [ "required", { "nested_object" : {
            "email"    : [ "required", "string", "email", "to_lc" ],
            "password" : [ "required", "string" ]
        } } ]
    };

    async execute({ data }) {
        const user = await User.findOne({ where: { email: data.email } });

        if (!(user && user.passwordHash && (await user.checkPassword(data.password)))) {
            throw new Exception({
                code   : "AUTHENTICATION_FAILED",
                fields : {
                    email    : "INVALID",
                    password : "INVALID"
                }
            });
        }

        return { token: jwt.sign(userDump(user), config.SECRET) };
    }
}
