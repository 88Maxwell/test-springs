import { Service } from "koa-service-layer";
import Exception from "koa-service-layer/dist/Exception";

import { userDump } from "../../utils/services/dumps";
import sequelize from "../../sequelize";

const User = sequelize.model("User");

export default class Create extends Service {
    static validate = {
        id : [ "required", "string" ]
    };

    async execute({ id }) {
        const user = await User.findByPk(id);

        if (!user) {
            throw new Exception({
                code   : "WRONG_ID",
                fields : {
                    id : "WRONG_ID"
                }
            });
        }
        const destroyedUser = await user.destroy();

        return userDump(destroyedUser);
    }
}
