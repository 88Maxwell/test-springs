import { Service } from "koa-service-layer";
import { userDump } from "../../utils/services/dumps";
import sequelize from "../../sequelize";

const User = sequelize.model("User");

export default class Create extends Service {
    static validate = {
        data : [ "required", { "nested_object" : {
            "password" : [ "required", "string" ],
            "name"     : [ "required", "string" ],
            "email"    : [ "required", "string" ]
        } } ]
    }

    async execute({ data }) {
        const user = await User.create(data);

        return userDump(user);
    }
}
