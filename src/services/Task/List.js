import { Service } from "koa-service-layer";
import { taskDump } from "../../utils/services/dumps";
import sequelize from "../../sequelize";

const Task = sequelize.model("Task");

export default class Create extends Service {
    static permissionCheck = true;
    static validate = {
        userId : [ "required", "string" ]
    }

    async execute({ userId }) {
        const task = await Task.findAll({ where: { userId } });

        return task.map(taskDump);
    }
}
