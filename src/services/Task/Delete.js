import { Service } from "koa-service-layer";
import Exception from "koa-service-layer/dist/Exception";

import { taskDump } from "../../utils/services/dumps";
import sequelize from "../../sequelize";

const Task = sequelize.model("Task");

export default class Create extends Service {
    static permissionCheck = true;
    static validate = {
        id : [ "required", "string" ]
    };

    async execute({ id }) {
        const task = await Task.findByPk(id);

        if (!task) {
            throw new Exception({
                code   : "WRONG_ID",
                fields : {
                    id : "WRONG_ID"
                }
            });
        }
        const destroyedTask = await task.destroy();

        return taskDump(destroyedTask);
    }
}
