import { Service } from "koa-service-layer";
import { taskDump } from "../../utils/services/dumps";
import sequelize from "../../sequelize";

const Task = sequelize.model("Task");

export default class Create extends Service {
    static permissionCheck = true;
    static validate = {
        userId : [ "required", "string" ],
        data   : [ "required", { "nested_object" : {
            "text" : [ "required", "string" ]
        } } ]
    }

    async execute({ data, userId }) {
        const task = await Task.create({ ...data, userId });

        return taskDump(task);
    }
}
