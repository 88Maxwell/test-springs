export default function (sequelize, Sequelize) {
    const Task = sequelize.define("Task", {
        id     : { type: Sequelize.UUID, defaultValue: Sequelize.UUIDV4, primaryKey: true },
        userId : { type: Sequelize.UUID, allowNull: false, references: { model: "User", key: "id" } },
        text   : { type: Sequelize.STRING },
        status : { type: Sequelize.ENUM, values: [ "SUCCESSFUL", "FAILED", "PENDING" ], defaultValue: "PENDING" }
    });

    // eslint-disable-next-line
    const initRelation = function () {
        const User = sequelize.model("User");

        this.belongsTo(User, { foreignKey: "id" });
    };

    return Task;
}
