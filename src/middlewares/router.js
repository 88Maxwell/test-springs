import Router from "koa-router";
import ServiceLayer  from "../serviceLayer";

import config from "../../configs/config.json";

import UserCreate from "../services/User/Create";
import UserDelete from "../services/User/Delete";
import UserLogin from "../services/User/Login";

import TaskList from "../services/Task/List";
import TaskCreate from "../services/Task/Create";
import TaskDelete from "../services/Task/Delete";
import TaskUpdate from "../services/Task/Update";


const router = new Router({ prefix: config.PREFIX });

router.post("/user", ServiceLayer.useService(UserCreate));
router.post("/user/login", ServiceLayer.useService(UserLogin));
router.delete("/user/:id", ServiceLayer.useService(UserDelete));

router.get("/user/:user_id/tasks", ServiceLayer.useService(TaskList));
router.post("/user/:user_id/task", ServiceLayer.useService(TaskCreate));
router.delete("/user/:user_id/task/:id", ServiceLayer.useService(TaskDelete));
router.patch("/user/:user_id/task/:id", ServiceLayer.useService(TaskUpdate));

export default router;
