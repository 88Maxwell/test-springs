import { ServiceLayer } from "koa-service-layer";
import rules from "./utils/serviceLayerRules";

export default new ServiceLayer({ rules });
