import Sequelize from "sequelize";
import dbConfig from "../configs/db.json";

import utils from "./utils/models";

const { database, username, password, dialect, host, port } = dbConfig[process.env.MODE];
const sequelize = new Sequelize(database, username, password, {
    insecureAuth     : true,
    host,
    port,
    dialect,
    logging          : false,
    operatorsAliases : utils.operatorsAliases
});

utils.init(sequelize, Sequelize);

export default sequelize;
