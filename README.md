# ToDo[backend]

## Initialize

### DEVELOPMENT
- yarn 
- install and run mysql-server service
- cp db.json.sample db.json && cp config.json.sample
- setup config.json and db.json
- yarn db:create development
- yarn db:migrate
- yarn run:dev
  
### TEST
- yarn 
- install and run mysql-server service
- cp db.json.sample db.json && cp config.json.sample
- setup config.json and db.json
- yarn db:create test
- yarn db:migrate
- yarn test
  
### PRODUCTION
- yarn 
- install and run mysql-server service
- cp db.json.sample db.json && cp config.json.sample
- setup config.json and db.json
- yarn db:create production
- yarn db:migrate
- yarn build
- node ./build/main.js

## Comments

This project uses [Koa-Service-Layer](https://github.com/88Maxwell/Koa-Service-Layer) (hereinafter just `KSL`) . This is the implementation of the [Service-Layer Template for Koa](http://design-pattern.ru/patterns/service-layer.html), which was written by me. By the way, it was  made by an inspirational implementation for `express` - [Node-Chista](https://github.com/koorchik/node-chista), which was used at my old workplace (outsourcing company `Webbylab`). There is no functional in the implementation of the work of the logger at the moment. It is important to say that the process on writing abstraction will be improved in the near future. It will allow the use of implementation with different kinds of libraries, and not only with `Koa`.

Without using `KSL`, you can replace the rules for midlewares:

- `hidden` rules ----> app.use (rule)
- `required` rules ----> use the function in the service, or just using the function or midleware of this type
```javascript
    args => (ctx, next)=> {
        /* 
        rule body
        use a scope for get args
        */
        return next()
    }
```

- `custom rules` ----> adding to the route when it is needed, or just using the function or midleware of this type

`KSL` allows you to create a single interface for writing an `API`, which in conjunction with `MVC` greatly simplifies the work.

