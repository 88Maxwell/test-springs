import { request, cleanup, matchLIVR } from "../..";
import { create, createLoginExpect, wrongAuthDataExpect } from "../../mocks/user";
import { generateUser } from "../../utils/user";

suite("Login user");

before(async () => {
    await generateUser();
});

test("Positive : Login user", async () => {
    await request
        .post("/api/v1/user/login")
        .send(create)
        .expect(200)
        .expect(res => matchLIVR(res.body, createLoginExpect));
});

test("Negative : Login user with wrong password", async () => {
    await request
        .post("/api/v1/user/login")
        .send({ data: { ...create.data, password: "wrong-password" } })
        .expect(200)
        .expect(res => matchLIVR(res.body, wrongAuthDataExpect));
});

after(async () => {
    await cleanup();
});
