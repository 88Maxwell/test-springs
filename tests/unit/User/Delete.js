import { request, cleanup, matchLIVR } from "../..";
import { wrongIdExpect } from "../../mocks";
import { createExpect } from "../../mocks/user";
import { generateUser } from "../../utils/user";

suite("Delete User");

let user;

before(async () => {
    user = await generateUser();
});

test("Positive : User Delete", async () => {
    await request
        .delete(`/api/v1/user/${user.id}`)
        .send()
        .expect(200)
        .expect(res => matchLIVR(res.body, createExpect));
});

test("Negative : Delete User with unexsited id", async () => {
    await request
        .delete("/api/v1/user/1234-4322-1234-5454")
        .send()
        .expect(200)
        .expect(res => matchLIVR(res.body, wrongIdExpect));
});

after(async () => {
    await cleanup();
});
