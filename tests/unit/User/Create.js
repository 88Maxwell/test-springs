import { request, cleanup, matchLIVR } from "../..";
import { create, createExpect } from "../../mocks/user";
import { emptyData, emptyDataExpect } from "../../mocks";

suite("Create User");

test("Positive : User create", async () => {
    await request
        .post("/api/v1/user")
        .send(create)
        .expect(200)
        .expect(res => matchLIVR(res.body, createExpect));
});

test("Positive : Empty data", async () => {
    await request
        .post("/api/v1/user")
        .send(emptyData)
        .expect(200)
        .expect(res => matchLIVR(res.body, emptyDataExpect));
});

after(async () => {
    await cleanup();
});
