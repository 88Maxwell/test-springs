import { request, cleanup, matchLIVR } from "../..";
import { listExpect } from "../../mocks/task";
import { create as userCreate } from "../../mocks/user";
import { generateUser, getToken } from "../../utils/user";
import { generateTasks } from "../../utils/task";

suite("List Task");

let user;

let token;

before(async () => {
    user = await generateUser();
    token = await getToken(userCreate);

    await generateTasks(user, 3);
});

test("Positive : Task list", async () => {
    await request
        .get(`/api/v1/user/${user.id}/tasks`)
        .set({ "X-AuthToken": token })
        .send()
        .expect(200)
        .expect(res => matchLIVR(res.body, listExpect));
});

after(async () => {
    await cleanup();
});
