import { request, cleanup, matchLIVR } from "../..";
import { updateToSuccessful, createExpect } from "../../mocks/task";
import { create as userCreate } from "../../mocks/user";
import { generateUser, getToken } from "../../utils/user";
import { generateTask } from "../../utils/task";

suite("Update Task");

let user;

let task;

let token;

before(async () => {
    user = await generateUser();
    task = await generateTask(user);
    token = await getToken(userCreate);
});

test("Positive : Task update", async () => {
    await request
        .delete(`/api/v1/user/${user.id}/task/${task.id}`)
        .set({ "X-AuthToken": token })
        .send(updateToSuccessful)
        .expect(200)
        .expect(res => matchLIVR(res.body, createExpect));
});

after(async () => {
    await cleanup();
});
