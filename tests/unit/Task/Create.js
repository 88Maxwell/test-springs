import { request, cleanup, matchLIVR } from "../..";
import { create, createExpect } from "../../mocks/task";
import { create as userCreate, permissionDeniedExpect } from "../../mocks/user";
import { generateUser, getToken } from "../../utils/user";

suite("Create Task");

let user;

let token;

before(async () => {
    user = await generateUser();
    token = await getToken(userCreate);
});

test("Positive : Task create", async () => {
    await request
        .post(`/api/v1/user/${user.id}/task`)
        .set({ "X-AuthToken": token })
        .send(create)
        .expect(200)
        .expect(res => matchLIVR(res.body, createExpect));
});

test("Negative : Create task without token ", async () => {
    await request
        .post(`/api/v1/user/${user.id}/task`)
        .send(create)
        .expect(200)
        .expect(res => matchLIVR(res.body, permissionDeniedExpect));
});


after(async () => {
    await cleanup();
});
