import { request } from "../";
import { userDump } from "../../src/utils/services/dumps";
import { create } from "../mocks/user";
import sequelize from "../../src/sequelize";

const User = sequelize.model("User");

export async function generateUser(name = "test") {
    const user = await User.create({ ...create.data, name });

    return userDump(user);
}

export async function generateUsers(count = 1) {
    const userList = [];

    // eslint-disable-next-line
    for (let index = 0; index < count; index++) {
        userList.push(await generateUser(`test ${index}`));
    }

    return userList;
}

export const getToken = async userCreate =>
    new Promise(resolve => request
        .post("/api/v1/user/login")
        .send(userCreate)
        .end((err, res) => {
            if (err) throw err;

            if (res.body.status === 500)  {
                throw res.body.error;
            }

            resolve(res.body.data.token);
        }));
