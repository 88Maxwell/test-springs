import { taskDump } from "../../src/utils/services/dumps";
import { create } from "../mocks/task";
import sequelize from "../../src/sequelize";

const Task = sequelize.model("Task");

export async function generateTask(user) {
    const task = await Task.create({ ...create.data, userId: user.id });

    return taskDump(task);
}


export async function generateTasks(user, count = 1) {
    const taskList = [];

    // eslint-disable-next-line
    for (let index = 0; index < count; index++) {
        taskList.push(await generateTask(user));
    }

    return taskList;
}
