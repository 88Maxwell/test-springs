export const create = {
    data : {
        name     : "test",
        email    : "test@gmail.com",
        password : "password"
    }
};

export const createExpect = {
    status : { "is": 200 },
    data   : [ "required", { "nested_object" : {
        id    : [ "required" ],
        name  : [ "required", "string" ],
        email : [ "required", "string" ]
    } } ]
};

export const listExpect = {
    status : { "is": 200 },
    data   : [ "required", { "list_of_objects" : {
        id    : [ "required" ],
        name  : [ "required", "string" ],
        email : [ "required", "string" ]
    } } ]
};

export const createLoginExpect = {
    status : { "is": 200 },
    data   : [ "required", { "nested_object" : {
        token : [ "required", "string" ]
    } } ]
};

export const wrongAuthDataExpect = {
    status : { "is": 500 },
    error  : [ "required", { "nested_object" : {
        code   : { "is": "AUTHENTICATION_FAILED" },
        fields : [ "required", { "nested_object" : {
            email    : { "is": "INVALID" },
            password : { "is": "INVALID" }
        } } ]
    } } ]
};


export const permissionDeniedExpect = {
    status : { "is": 500 },
    error  : [ "required", { "nested_object" : {
        code   : { "is": "PERMISSION_DENIED" },
        fields : [ "required", { "nested_object" : {
            token : { "is": "WRONG_TOKEN" }
        } } ]
    } } ]
};
