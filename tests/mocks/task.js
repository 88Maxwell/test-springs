export const create = {
    data : {
        text : "test"
    }
};

export const createExpect = {
    status : { "is": 200 },
    data   : [ "required", { "nested_object" : {
        id     : [ "required" ],
        userId : [ "required" ],
        text   : [ "required", "string" ],
        status : [ "string" ]
    } } ]
};

export const listExpect = {
    status : { "is": 200 },
    data   : [ "required", { "list_of_objects" : {
        id     : [ "required" ],
        userId : [ "required" ],
        text   : [ "required", "string" ],
        status : [ "string" ]
    } } ]
};

export const updateToSuccessful = {
    data : {
        text   : "updated_test",
        status : "SUCCESSFUL"
    }
};

export const updateToSuccessfulExpect = {
    status : { "is": 200 },
    data   : [ "required", { "nested_object" : {
        id     : [ "required" ],
        userId : [ "required" ],
        text   : { "is": "updated_test" },
        status : { "is": "SUCCESSFUL" }
    } } ]
};

